package ee.valiit.companiesapi.repository;

import ee.valiit.companiesapi.model.Company;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CompanyRepository {

    //sõltuvuse jaoks vajalik ligipääs ühele oale
    @Autowired
    private JdbcTemplate jbdcTemplate;

    public List<Company> fetchCompanies() {
        //igale reale rakendatakse järgnevat lambda-avaldist:
        List<Company> companies = jbdcTemplate.query(
                "SELECT * FROM company",
                (row, rowNum) -> {
                    return new Company( //kuidas objekt üles ehitada
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                }
        );
        return companies;
    }

    // meetod, mis tagastaks konkreetse ettevõtte andmed vastavalt id-le
    public Company fetchCompany(int id) {
        List<Company> companies = jbdcTemplate.query(
                "SELECT * FROM company WHERE id = ?", // ? on nö placeholder
                new Object[] { id },
                (row, rowNum) -> {
                    return new Company( //kuidas objekt üles ehitada
                            row.getInt("id"),
                            row.getString("name"),
                            row.getString("logo")
                    );
                }
        );
        //lühem variant: return companies.size() > 0 ? companies.get(0) : null;

        if (companies.size() > 0) {
            return companies.get(0);
        } else {
            return null;
        }
    }

    public void deleteCompany(int id) {
        jbdcTemplate.update("DELETE FROM company WHERE id = ?", id);
    }

    public void addCompany(Company company) {
        jbdcTemplate.update("INSERT INTO company (name, logo) VALUES (?, ?)", company.getName(), company.getLogo());
    }

    public void updateCompany(Company company) {
        jbdcTemplate.update("UPDATE company SET name = ?, logo = ? WHERE id = ?",
                company.getName(), company.getLogo(), company.getId());
    }
}
