package ee.valiit.companiesapi.controller;

import ee.valiit.companiesapi.model.Company;
import ee.valiit.companiesapi.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@CrossOrigin("*") //muidu ei vasta server mujalt tulnud päringutele
public class CompaniesController {

    @Autowired
    private CompanyRepository companyRepository; //dependency injection

    @GetMapping("/companies")
    public List<Company> getCompanies() {
        return companyRepository.fetchCompanies();
    }

    @GetMapping("/company")
    public Company getCompany(@RequestParam("id") int id) {
        return companyRepository.fetchCompany(id);
    }

    @DeleteMapping("/company")
    public void deleteCompany(@RequestParam("id") int id) {
        companyRepository.deleteCompany(id);
    }

    // lisame firma c andmed
    @PostMapping("/company")
    public void addCompany(@RequestBody Company c) { companyRepository.addCompany(c); }

    // muudame firma x andmeid
    @PutMapping("/company")
    public void updateCompany(@RequestBody Company x) {
        companyRepository.updateCompany(x);
    }
}
