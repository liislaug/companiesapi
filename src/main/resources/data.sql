INSERT INTO `company` (`name`, `logo`) VALUES
	('Arco Vara', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=ARC'),
	('LHV Group', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=LHV'),
	('Random Named Masterpiece', 'https://freelogo-assets.s3.amazonaws.com/assets/images/CRNM.gif'),
	('Merko Ehitus', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=MRK'),
	('Apranga', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=APG'),
	('Baltika', 'https://www.nasdaqbaltic.com/market/logo.php?issuer=BLT');